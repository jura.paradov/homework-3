import numpy as np


def make_matrix(n, type_of):
    return np.empty((n, n), dtype=type_of)


def read_matrix(n):
    new_matrix = []
    for i in range(n):
        new_matrix.append(list(map(int, input().split())))
    return np.array(new_matrix)


def add_zeros_to_matrix(matrix, new_size):
    size = matrix.shape[0]
    type_of = type(matrix[0][0])
    matrix = np.hstack((matrix, np.zeros((size, new_size - size), dtype=type_of)))
    matrix = np.vstack((matrix, np.zeros((new_size - size, new_size), dtype=type_of)))
    return matrix


def multiply(A, B, res):
    if A.size == 1:
        return A * B
    half_size = A.shape[0] // 2
    a1_1 = A[:half_size, :half_size]
    a1_2 = A[:half_size, half_size:]
    a2_1 = A[half_size:, :half_size]
    a2_2 = A[half_size:, half_size:]
    b1_1 = B[:half_size, :half_size]
    b1_2 = B[:half_size, half_size:]
    b2_1 = B[half_size:, :half_size]
    b2_2 = B[half_size:, half_size:]
    p1 = multiply(a1_1 + a2_2, b1_1 + b2_2, make_matrix(half_size, type(A[0][0])))
    p2 = multiply(a2_1 + a2_2, b1_1, make_matrix(half_size, type(A[0][0])))
    p3 = multiply(a1_1, b1_2 - b2_2, make_matrix(half_size, type(A[0][0])))
    p4 = multiply(a2_2, b2_1 - b1_1, make_matrix(half_size, type(A[0][0])))
    p5 = multiply(a1_1 + a1_2, b2_2, make_matrix(half_size, type(A[0][0])))
    p6 = multiply(a2_1 - a1_1, b1_1 + b1_2, make_matrix(half_size, type(A[0][0])))
    p7 = multiply(a1_2 - a2_2, b2_1 + b2_2, make_matrix(half_size, type(A[0][0])))
    res[:half_size, :half_size] = p1 + p4 - p5 + p7
    res[:half_size, half_size:] = p3 + p5
    res[half_size:, :half_size] = p2 + p4
    res[half_size:, half_size:] = p1 - p2 + p3 + p6
    return res


def strassen(A, B):
    size = A.shape[0]
    pow_2 = 2**((size - 1).bit_length())
    A = add_zeros_to_matrix(A, pow_2)
    B = add_zeros_to_matrix(B, pow_2)
    result = make_matrix(pow_2, type(A[0][0]))
    result = multiply(A, B, result)
    return result[:size, :size]


def print_matrix(C):
    size = C.shape[0]
    for i in range(size):
        print(*C[i])


n = int(input())
matrix1, matrix2 = read_matrix(n), read_matrix(n)
print_matrix(strassen(matrix1, matrix2))
